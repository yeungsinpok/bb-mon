const socket = io.connect();
const videoGrid = document.getElementById('video-grid');
const myPeer = new Peer(undefined, {
  host: '/',
  port: '3001'
});

window.onload = () => {
  getUserWarning();
  getUserInfo();
};

myPeer.on('open', id => {
  console.log("1. join the room");
  socket.emit('join-room', id);
});

navigator.mediaDevices.getUserMedia({
  video: true
}).then(stream => {
  console.log("inside the stream promise");
  myPeer.on('call', call => {
    console.log("3. answer baby's call")
    call.answer();
    const video = document.createElement('video');
    call.on('stream', userVideoStream => {
      console.log("Mommy is going to generate baby's video");
      addVideoStream(video, userVideoStream);
    });
  });

});

socket.on('user-disconnected', userId => {
  document.querySelector('video').remove();
});



function addVideoStream(video, stream) {
  video.srcObject = stream;
  video.addEventListener('loadedmetadata', () => {
    video.play();
    videoGrid.append(video);
  });
};


let suffocationPopup = document.querySelector(".suffocationPopup");
let leavePopup = document.querySelector(".leavePopup");
let cryPopup = document.querySelector(".cryPopup");


let suffocationIcon = document.querySelector(".suffocationPopup .suffocationIcon");
let suffocationTime = document.querySelector(".suffocationPopup .suffocationTime");
let suffocationWarning = document.querySelector(".suffocationPopup .suffocationDescription");

let leaveIcon = document.querySelector(".leavePopup .leaveIcon");
let leaveTime = document.querySelector(".leavePopup .leaveTime");
let leaveWarning = document.querySelector(".leavePopup .leaveDescription");

let cryIcon = document.querySelector(".cryPopup .cryIcon");
let cryTime = document.querySelector(".cryPopup .cryTime");

// Show warning message

socket.on('warning_message', (warningMessages) => {
  console.log(warningMessages);
  for (let warningMessage of warningMessages) {
    if (warningMessage['baby_status_id'] === 1) {
      let suffocationWarningTime = dateFns.format(new Date(warningMessage['created_at']), "HH:mm");
      suffocationTime.innerHTML = suffocationWarningTime;
      suffocationWarning.innerHTML = warningMessage['warning_message'];
      if (warningMessage['level'] === 1) {
        suffocationPopup.classList.add("active");
        suffocationTime.classList.remove("red");
        suffocationIcon.classList.remove("red");
        suffocationTime.classList.add("yellow");
        suffocationIcon.classList.add("yellow");
      }
      else {
        suffocationPopup.classList.add("active");
        suffocationTime.classList.remove("yellow");
        suffocationTime.classList.remove("yellow");
        suffocationIcon.classList.add("red");
        suffocationTime.classList.add("red");
      }

    } else if (warningMessage['baby_status_id'] === 2) {
      let offTheBedWarningTime = dateFns.format(new Date(warningMessage['created_at']), "HH:mm");
      leaveTime.innerHTML = offTheBedWarningTime;
      leaveWarning.innerHTML = warningMessage['warning_message'];
      if (warningMessage['level'] === 1) {
        leavePopup.classList.add("active");
        leaveIcon.classList.remove("red");
        leaveTime.classList.remove("red");
        leaveIcon.classList.add("yellow");
        leaveTime.classList.add("yellow");
      }
      else {
        leavePopup.classList.add("active");
        leaveIcon.classList.remove("yellow");
        leaveTime.classList.remove("yellow");
        leaveIcon.classList.add("red");
        leaveTime.classList.add("red");
      }
    }
    else {
      let cryWarningTime = dateFns.format(new Date(warningMessage['created_at']), "HH:mm");
      cryTime.innerHTML = cryWarningTime;
      cryPopup.classList.add("active");
      cryIcon.classList.add("active");
    }
  }
});

//-------------




// get id



let allInfo = document.querySelector('.allInfo');

let time = document.querySelector('.time');
let babyStatus = document.querySelector('.status');
let level = document.querySelector('.level');
async function getUserWarning() {
  const res = await fetch('/message');
  const message = await res.json();
  for (let row of message) {
    allInfo.innerHTML +=

      `<div class="time"><span class="month">${dateFns.format(new Date(row['created_at']), "DD MMM")}</span>(${dateFns.format(new Date(row['created_at']), "HH:mm")})
  <span class="status">${row['baby_status']}</span></div>
  <div class="warningMessage">${row['warning_message']}</div>`;
  }



}



///logout
const logout = document.querySelector('#logout');

logout.onclick = async function () {
  const res = await fetch('/logout');
  if (res.status === 200) {
    window.location = '/index.html'
  } else {
    window.location = '/404.html'
  }
};


// userinfo
let headerUsername = document.querySelector(".headerUsername");


async function getUserInfo() {
  const res = await fetch('/getUserInfo');
  const userInfo = await res.json();
  headerUsername.innerHTML += userInfo['userInfo']['username']
}