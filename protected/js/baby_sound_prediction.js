const recordAudio = () =>

  new Promise(async resolve => {
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });
    //console.log(stream)
    const mediaRecorder = new MediaRecorder(stream);
    const audioChunks = [];

    mediaRecorder.addEventListener("dataavailable", event => {
      audioChunks.push(event.data);
    });

    //start() and stop() will be called in the loop function below, here is just a setup closure for both functions
    const start = () => mediaRecorder.start();

    const stop = () =>
      new Promise(resolve => {
        mediaRecorder.addEventListener("stop", async () => {
          const audioBlob = new Blob(audioChunks);
          let reader = new FileReader();
          reader.readAsDataURL(audioBlob);
          reader.onload = async () => {
            const base64AudioMessage = reader.result.split(',')[1];
            const res = await fetch('/sound_prediction',
              {
                method: "POST",
                headers: {
                  "Content-Type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({
                  babySound: base64AudioMessage
                })
              }
            )
          const result = await res.json();
          if (res.status == 200) {
            console.log(result);
            // console.log("Baby side copy received server response");
          }
          resolve();
          }
        });

        mediaRecorder.stop();

      });

    resolve({ start, stop });

  });


export const sleep = time => new Promise(resolve => setTimeout(resolve, time));

//Below function is actually running

export async function babySoundPrediction() {
  let recorder = await recordAudio();
  recorder.start();
  await sleep(6000);
  await recorder.stop(); 
}





///logout 
const logout = document.querySelector('.logout');

logout.onclick = async function () {
    const res = await fetch('/logout');
    if (res.status === 200) {
        window.location = '/index.html'
    } else {
        window.location = '/404.html'
    }
};
