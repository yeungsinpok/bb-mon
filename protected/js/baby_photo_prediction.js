// --- WebRTC with socket.io

import { babySoundPrediction, sleep } from './baby_sound_prediction.js';

const socket = io.connect();
const videoGrid = document.getElementById('video-grid');
const myPeer = new Peer(undefined, {
  host: '/',
  port: '3001'
})
const myVideo = document.createElement('video');

window.onload = () => {
  getUserInfo();
};

myPeer.on('open', id => {
  console.log("1. join the room");
  socket.emit('join-room', id);
});

let myVideoStream;
navigator.mediaDevices.getUserMedia({
  video: true
}).then(stream => {
  myVideoStream = stream;
  addVideoStream(myVideo, myVideoStream);

  socket.on('user-connected', userId => {
    console.log('User connected! user_id: ', userId);
    console.log('2. make the call');
    setTimeout (()=>{
      myPeer.call(userId, myVideoStream);
    },2000);
  })
});


function addVideoStream(video, stream) {
  video.srcObject = stream
  video.addEventListener('loadedmetadata', () => {
    video.play();
    videoGrid.append(video);
  });
};

// ---- Body pix prediction 

const searchParams = new URLSearchParams(location.search);
const babyFaceInitialInfo = searchParams.toString();
console.log(babyFaceInitialInfo);
const originalFaceArea = searchParams.getAll("1");
const originalNoseX = searchParams.getAll("2");
const originalNoseY = searchParams.getAll("3");
const originalLeftEyeX = searchParams.getAll("4");
const originalLeftEyeY = searchParams.getAll("5");
const originalRightEyeX = searchParams.getAll("6");
const originalRightEyeY = searchParams.getAll("7");

console.log("original Face Area: " + originalFaceArea);
console.log("original Nose X: " + originalNoseX);
console.log("original Nose Y: " + originalNoseY);
console.log("original LeftEye X: " + originalLeftEyeX);
console.log("original LeftEye Y: " + originalLeftEyeY);
console.log("original RightEye X: " + originalRightEyeX);
console.log("original RightEye Y: " + originalRightEyeY);

let canvas = document.querySelector("#canvas");
let testButton = document.querySelector("#test_button");


const babyBodyPrediction = async () => {
  await sleep(2000);
  canvas.getContext('2d').drawImage(myVideo, 0, 0, canvas.width, canvas.height);
  canvas.style.display = 'none';
  let image = document.getElementById('canvas');

  const net = await bodyPix.load({
    architecture: 'MobileNetV1',
    outputStride: 8,
    multiplier: 1.0,
    quantBytes: 4
  });

  const segmentation = await net.segmentMultiPersonParts(image, {
    flipHorizontal: false,
    internalResolution: 'medium',
    segmentationThreshold: 0.7,
    maxDetections: 10,
    scoreThreshold: 0.2,
    nmsRadius: 20,
    minKeypointScore: 0.3,
    refineSteps: 10
  });

  // console.log("segmentation: ", segmentation);

  let faceArea = 0;
  let leftFaceArea = 0;
  let rightFaceArea = 0;
  let noseX, noseY, noseScore, leftEyeScore, rightEyeScore, leftEarScore, rightEarScore = 0;

  let warning = {
    suffocation: 0,
    offTheBed: 0
  }


  if (segmentation['length'] != 0) {
    for (let bodyPartPixel of segmentation['0']['data']) {
      if (bodyPartPixel == 0) {
        leftFaceArea++;
        faceArea++;
      }
      else if (bodyPartPixel == 1) {
        rightFaceArea++;
        faceArea++;
      }
    }

    //Nose  XY point
    noseX = segmentation['0']['pose']['keypoints']['0']['position']['x'];
    noseY = segmentation['0']['pose']['keypoints']['0']['position']['y'];
    //Score
    noseScore = segmentation['0']['pose']['keypoints']['0']['score'];
    leftEyeScore = segmentation['0']['pose']['keypoints']['1']['score'];
    rightEyeScore = segmentation['0']['pose']['keypoints']['2']['score'];
    leftEarScore = segmentation['0']['pose']['keypoints']['3']['score'];
    rightEarScore = segmentation['0']['pose']['keypoints']['4']['score'];

   

    if ((faceArea / originalFaceArea) < 0.7 && noseScore < 0.6) {
      console.log('Suffocation red warning!');
      warning['suffocation'] = 2;
    }
    else if ((faceArea / originalFaceArea) < 0.8 || noseScore < 0.8) {
      console.log('Suffocation yellow warning!');
      warning['suffocation'] = 1;
    }
    else {
      warning['suffocation'] = 0;
    }

    if (noseX < 40 || noseX > 600) {
      console.log('Baby is highly likely fall off the bed');
      warning['offTheBed'] = 2;
    }
    else if (noseX < 120 || noseX > 520) {
      console.log('Baby could fall off the bed');
      warning['offTheBed'] = 1;
    }
    else if (noseX >= 120 && noseX <= 520) {
      warning['offTheBed'] = 0;
    }

  } else {
    console.log("Could not detect any human face!");
    warning['suffocation'] = 2;
    warning['offTheBed'] = 2;
  }
  console.log('Suffocation warning: ', warning['suffocation']);
  console.log('Off the bed warning: ', warning['offTheBed']);


  if (warning['suffocation'] != 0 || warning['offTheBed'] != 0) {
    const res = await fetch('/photo_prediction', {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(warning)
    });
    const result = await res.json();

    if (res.status == 200) {
      console.log(result);
    }
    else if (res.status == 500) {
      console.log(result);
    }
  }
}


async function repeatRunPrediction() {
  await babyBodyPrediction();
  await babySoundPrediction();
  setTimeout(repeatRunPrediction, 1);
}

repeatRunPrediction();




// userinfo

let username = document.querySelector(".name");
let parent = document.querySelector(".parent");
let phone = document.querySelector(".phone");
let email = document.querySelector(".email");
let headerUsername = document.querySelector(".headerUsername");


async function getUserInfo() {
  const res = await fetch('/getUserInfo');
  const userInfo = await res.json();
  username.innerHTML += userInfo['userInfo']['username'];
  parent.innerHTML += userInfo['userInfo']['given_name'] + ' ' + userInfo['userInfo']['family_name']
  phone.innerHTML += userInfo['userInfo']['phone']
  email.innerHTML += userInfo['userInfo']['email']
  headerUsername.innerHTML += userInfo['userInfo']['username']
}



// messagebox
let messageBox = document.querySelector(".messageBox");
let ok = document.querySelector(".ok");


ok.addEventListener("click", function () {
  messageBox.classList.add("active");
});

