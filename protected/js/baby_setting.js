
let startCamera = document.querySelector("#start-camera");
let video = document.querySelector("#video");
let bodyMesh = document.querySelector("#body_mesh").getContext("2d");
let clickButton = document.querySelector("#click-photo");
let canvas = document.querySelector("#canvas");
let image;
let remind = document.querySelector(".remind");
let remindimg = document.querySelector(".remindimg");
let remindBox = document.querySelector(".remind_box");
// let clock= document.querySelector(".clock");
let successPopup = document.querySelector(".successPopup")
let closePopup = document.querySelector(".dismiss-btn");
let popup = document.querySelector(".popup");
let methodBlock = document.querySelector(".methodBlock");
let nextPage = document.querySelector(".nextPage");





startCamera.addEventListener('click', async function () {
  // startCamera.style.display = 'none';
  let stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false });
  video.srcObject = stream;
  console.log("this is front end");
  video.addEventListener("resize", (event) => {
    bodyMesh.width = video.videoWidth;
    bodyMesh.height = video.videoHeight;
  });

  video.addEventListener("loadedmetadata", async (event) => {
    bodyMesh.fillStyle = "white";
    bodyMesh.fillRect(160, 0, 10, 1000);
    bodyMesh.fillRect(460, 0, 10, 1000);
    bodyMesh.fillRect(0, 300, 1000, 10);
    remind.classList.add('active');
    remindimg.classList.add('active');
    remindBox.classList.add('active');
  })

});

clickButton.addEventListener('click', async function () {
  canvas.getContext('2d').drawImage(video, 0, 0, canvas.width, canvas.height);
  image = document.getElementById('canvas');
  methodBlock.classList.add('active');

  const net = await bodyPix.load({
    architecture: 'MobileNetV1',
    outputStride: 8,
    multiplier: 1.0,
    quantBytes: 4
  });

  const segmentation = await net.segmentMultiPersonParts(image, {
    flipHorizontal: false,
    internalResolution: 'medium',
    segmentationThreshold: 0.7,
    maxDetections: 10,
    scoreThreshold: 0.2,
    nmsRadius: 20,
    minKeypointScore: 0.3,
    refineSteps: 10
  });

  console.log("segmentation: ", segmentation);

  let faceArea = 0;
  let leftFaceArea = 0;
  let rightFaceArea = 0;
  let noseX, noseY, noseScore, leftEyeScore, rightEyeScore, leftEarScore, rightEarScore = 0;
  
  console.log(segmentation['length']);

  if (segmentation['length'] != 0){
    for (let bodyPartPixel of segmentation['0']['data']) {
      if (bodyPartPixel == 0) {
        leftFaceArea++;
        faceArea++;
      }
      else if (bodyPartPixel == 1) {
        rightFaceArea++;
        faceArea++;
      }
    }

    //Nose  XY point
    noseX = segmentation['0']['pose']['keypoints']['0']['position']['x'];
    noseY = segmentation['0']['pose']['keypoints']['0']['position']['y'];
    //eye  XY point
    leftEyeX = segmentation['0']['pose']['keypoints']['1']['position']['x'];
    leftEyeY = segmentation['0']['pose']['keypoints']['1']['position']['y'];
    rightEyeX = segmentation['0']['pose']['keypoints']['2']['position']['x'];
    rightEyeY = segmentation['0']['pose']['keypoints']['2']['position']['y'];
    //ear  XY point
    leftEarX = segmentation['0']['pose']['keypoints']['3']['position']['x'];
    leftEarY = segmentation['0']['pose']['keypoints']['3']['position']['y'];
    rightEarX = segmentation['0']['pose']['keypoints']['4']['position']['x'];
    rightEarY = segmentation['0']['pose']['keypoints']['4']['position']['y'];
    //Score
    noseScore = segmentation['0']['pose']['keypoints']['0']['score'];
    leftEyeScore = segmentation['0']['pose']['keypoints']['1']['score'];
    rightEyeScore = segmentation['0']['pose']['keypoints']['2']['score'];
    leftEarScore = segmentation['0']['pose']['keypoints']['3']['score'];
    rightEarScore = segmentation['0']['pose']['keypoints']['4']['score'];
  
    // console.log in front end
 
    console.log("Face area is: ", faceArea);
    console.log("Left face area: ", leftFaceArea / faceArea * 100, "%");
    console.log("Right face area: ", rightFaceArea / faceArea * 100, "%");
  
    console.log('Nose x position: ', noseX);
    console.log('Nose y position: ', noseY);
    console.log('Nose score: ', noseScore);

    console.log('Left Eye x position: ', leftEyeX);
    console.log('Left Eye y position: ', leftEyeY);
    console.log('Left eye score: ', leftEyeScore);
   
    console.log('Right Eye x position: ', rightEyeX);
    console.log('Right Eye y position: ', rightEyeY);
    console.log('Right eye score: ', rightEyeScore);
 
    console.log('Left Ear x position: ', leftEarX);
    console.log('Left Ear y position: ', leftEarY);
    console.log('Left ear score: ', leftEarScore);
 
    console.log('Right Ear x position: ', rightEarX);
    console.log('Right Ear y position: ', rightEarY);
    console.log('Right ear score: ', rightEarScore);
    console.log(segmentation);
  
    // all number Math.floor
    // faceArea
    let IntegerLeftFaceArea = Math.floor(leftFaceArea / faceArea * 100)
    let IntegerRightFaceArea = Math.floor(rightFaceArea / faceArea * 100)
    // nose (XY, Score)
    let IntegerNoseX = Math.floor(noseX)
    let IntegerNoseY = Math.floor(noseY)
    let IntegerNoseScore = Math.floor(noseScore * 100)
    // left eye (XY, Score)  
    let IntegerLeftEyeX = Math.floor(leftEyeX)
    let IntegerLeftEyeY = Math.floor(leftEyeY)
    let IntegerLeftEyeScore = Math.floor(leftEyeScore * 100)
    // right eye (XY, Score)  
    let IntegerRightEyeX = Math.floor(rightEyeX)
    let IntegerRightEyeY = Math.floor(rightEyeY)
    let IntegerRightEyeScore = Math.floor(rightEyeScore * 100)
    // left ear (XY, Score)  
      // let IntegerLeftEarX = Math.floor(leftEarX)
      // let IntegerLeftEarY = Math.floor(leftEarY)
    // right ear (XY, Score)  
      // let IntegerRightEarX = Math.floor(rightEarX)
      // let IntegerRightEarY = Math.floor(rightEarY)

    
    if (faceArea < 18000 || IntegerLeftFaceArea < 30 || IntegerRightFaceArea < 30 || IntegerNoseX < 150|| IntegerNoseX > 500 || IntegerNoseY < 80 || IntegerNoseY > 300 || IntegerNoseScore < 90 || IntegerLeftEyeScore < 85 || IntegerRightEyeScore < 85 )  {
      popup.classList.add("active");
    } else {
      successPopup.classList.add("active");
      // send current number to URL and use in next page
      console.log('face success')
      let successlink = './baby_side.html?1=' + faceArea + '&2=' + IntegerNoseX + '&3=' + IntegerNoseY + "&4=" + IntegerLeftEyeX + "&5=" + IntegerLeftEyeY + "&6=" + IntegerRightEyeX + "&7=" + IntegerRightEyeY
      console.log(successlink)
      nextPage.setAttribute("href", successlink);
    }
  

  }else{
    console.log("Cannot detect any human face!");
    popup.classList.add("active");
  }

  clickButton.onclick = function () {
    remind.classList.remove('active');
    remindimg.classList.remove('active');
    remindBox.classList.remove('active');
  }
});



// window.onload = function realtimeClock() {
//   var rtClock = new Date();
//   var hours = rtClock.getHours();
//   var minutes = rtClock.getMinutes();
//   var seconds = rtClock.getSeconds();

//   var amPm = (hours < 12) ? "AM" : "PM";

//   hours = (hours > 12) ? hours - 12 : hours;

//   hours = ("0" + hours).slice(-2);
//   minutes = ("0" + minutes).slice(-2);
//   seconds = ("0" + seconds).slice(-2);

//   document.getElementById('clock').innerHTML = hours + ":" + minutes + ":" + seconds + " " + amPm;
//   setTimeout(realtimeClock, 500);
// }


// popup




closePopup.addEventListener("click",function(){
  popup.classList.remove("active");
});





///logout 
const logout = document.querySelector('.logout');

logout.onclick = async function () {
    const res = await fetch('/logout');
    if (res.status === 200) {
        window.location = '/index.html'
    } else {
        window.location = '/404.html'
    }
};

// userinfo


window.onload = () => {
  getUserInfo();
};


let headerUsername = document.querySelector(".headerUsername");


async function getUserInfo(){
  const res = await fetch('/getUserInfo');
  const userInfo = await res.json();
  headerUsername.innerHTML += userInfo['userInfo']['username']
}