import { Knex } from "knex";


export async function up(knex: Knex): Promise<void> {
    const hasUserInfoTable = await knex.schema.hasTable('user_info');
    if(!hasUserInfoTable){
        await knex.schema.createTable('user_info',(table)=>{
            table.increments();
            table.text("username").notNullable();
            table.text("password").notNullable();
            table.text("given_name").notNullable();
            table.text("family_name").notNullable();
            table.text("email").notNullable();
            table.text("phone").notNullable();
            table.timestamps(false,true);
        });  
    }

    const hasMessageTable = await knex.schema.hasTable('message');
    if(!hasMessageTable){
        await knex.schema.createTable('message',(table)=>{
            table.increments();
            table.text("content").notNullable();
            table.timestamps(false,true);
        });  
    }

    const hasBabyStatusTable = await knex.schema.hasTable('baby_status');
    if(!hasBabyStatusTable){
        await knex.schema.createTable('baby_status',(table)=>{
            table.increments();
            table.text("status").notNullable();
            table.timestamps(false,true);
        });  
    }

    const hasWarningInfoTable = await knex.schema.hasTable('warning_info');
    if(!hasWarningInfoTable){
        await knex.schema.createTable('warning_info',(table)=>{
            table.increments();
            table.integer("message_id").unsigned().notNullable();
            table.foreign("message_id").references("message.id");
            table.integer("level").notNullable();
            table.integer("baby_status_id").unsigned().notNullable();
            table.foreign("baby_status_id").references("baby_status.id");
            table.integer("user_info_id").unsigned().notNullable();
            table.foreign("user_info_id").references("user_info.id");
            table.timestamps(false,true);
        });  
    }
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists("warning_info");
    await knex.schema.dropTableIfExists("baby_status");
    await knex.schema.dropTableIfExists("message");
    await knex.schema.dropTableIfExists("user_info");
}

