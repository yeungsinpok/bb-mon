import express, { Router } from 'express';
//import { UserController } from './controllers/UserControllers';
// import { photoController, upload, userController } from './app';
import { photoController, userController, soundController, grantExpress, messageController} from './app';


import { isLoggedInAPI } from './guard';

export const routes = express.Router();

// google login
routes.use(grantExpress);
routes.get('/login/google', userController.loginGoogle);
routes.post('/register/google', userController.registerGoogle);
routes.get('/logout', userController.logout);
routes.post('/login', userController.login);
routes.post('/users', isLoggedInAPI, userController.createUser)

// You can put the photoRoutes here
routes.post('/photo_prediction', photoController.photoPredictionResult);

//mum_side get warn message from DB
routes.get('/message', isLoggedInAPI, messageController.getMessage);

//user info Routes
routes.get('/getUserInfo', isLoggedInAPI, userController.getUserInfo);

// soundRoutes
routes.post('/sound_prediction', soundController.predictionResult);

