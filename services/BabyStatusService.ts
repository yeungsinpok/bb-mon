// import { Client } from "pg";

// export class BabyStatusService{

//     constructor(private client:Client){}

//     async getStatus(id:number){
//         return await this.client.query('SELECT status FROM baby_status WHERE id = $1)', [id]);
//     }
// }

import { Knex } from 'knex';

export class BabyStatusService{
    constructor(private knex:Knex){}
    async getStatus(id:number){
        return await this.knex.select('status').from('baby_status').where('id',id)
    }
}