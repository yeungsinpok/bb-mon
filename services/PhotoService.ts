import { Knex } from "knex";
import { logger } from "../logger";

export class PhotoService{
    
    constructor(private knex:Knex){}

    async createWarningMessage(userId: number, suffocation: number, offTheBed: number){
        const txn = await this.knex.transaction();
        try {
            let warningInfoId: number[];
            if (suffocation != 0 && offTheBed != 0) {
                console.log("Two warning");
                warningInfoId = await txn.insert([
                    {message_id: suffocation, level: suffocation, baby_status_id: 1, user_info_id: userId},
                    {message_id: offTheBed + 2, level: offTheBed, baby_status_id: 2, user_info_id: userId}
                ]).into('warning_info').returning('id');
            }
            else if (suffocation != 0) {
                console.log("Suffocation only");
                warningInfoId = await txn.insert(
                    {message_id: suffocation, level: suffocation, baby_status_id: 1, user_info_id: userId}
                ).into('warning_info').returning('id');
            }
            else if (offTheBed != 0) {
                console.log('Off the bed only');
                warningInfoId = await txn.insert(
                    {message_id: offTheBed + 2, level: offTheBed, baby_status_id: 2, user_info_id: userId}
                ).into('warning_info').returning('id');
            }
            await txn.commit();
            return warningInfoId;
        }catch(e) {
            logger.error(e);
            await txn.rollback();
            return;
        }
    }

    async getWarningMessage(warningInfoIds: number[]) {
        try {
            return await this.knex('warning_info').select(['content as warning_message', 'level', 'baby_status_id','status as baby_status', 'warning_info.created_at as created_at']).innerJoin('message', 'message.id', 'warning_info.message_id').innerJoin('baby_status', 'baby_status.id', 'warning_info.baby_status_id').whereIn('warning_info.id', warningInfoIds);
        }catch(e) {
            logger.error(e);
            return;
        }
    }
}