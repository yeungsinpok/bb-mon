import { Knex } from "knex";
import { logger } from "../logger";

export class SoundService{
    
    constructor(private knex:Knex){}

    async createWarningMessage(userId: number){
        const txn = await this.knex.transaction();
        try {
            let warningInfoId: number[];
            
            console.log("Baby is crying in soundService");
            warningInfoId = await txn.insert(
                {message_id: 5, level: 1, baby_status_id: 3, user_info_id: userId}
            ).into('warning_info').returning('id');
            
            await txn.commit();
            return warningInfoId;
        }catch(e) {
            logger.error(e);
            await txn.rollback();
            return;
        }
    }

    async getWarningMessage(warningInfoIds: number[]) {
        try {
            return await this.knex('warning_info').select(['baby_status_id','status as baby_status', 'warning_info.created_at as created_at']).innerJoin('message', 'message.id', 'warning_info.message_id').innerJoin('baby_status', 'baby_status.id', 'warning_info.baby_status_id').whereIn('warning_info.id', warningInfoIds);
        }catch(e) {
            logger.error(e);
            return;
        }
    }
}