// import { Client } from "pg";

// export class MessageService{

//     constructor(private client:Client){}

//     async getMessage(id:number){
//         return await this.client.query('SELECT content FROM message WHERE id = $1)', [id]);
//     }
// }
import { logger } from '../logger';
import { Request, Response } from 'express';
import { Knex } from 'knex';
import { format } from 'date-fns';

export class MessageService{
    constructor(private knex:Knex){}
    
    async getWarningRecord(id:number){
        const today: Date = new Date(); 
        let stringToday = format(today, 'yyyy-MM-dd');    
        // return await this.knex.raw(`SELECT * From warning_info WHERE created_at >= TO_TIMESTAMP($1, 'yyyy-mm-dd') AND user_info_id = $2 order by created_at DESC;`, [stringToday,id])
        return await this.knex('warning_info').select(['warning_info.created_at as created_at', 'warning_info.id','status as baby_status', 'content as warning_message', 'warning_info.user_info_id']).innerJoin('message', 'message.id', 'warning_info.message_id').innerJoin('baby_status', 'baby_status.id', 'warning_info.baby_status_id').where('warning_info.user_info_id', id).andWhere('warning_info.created_at', '>=', stringToday).orderBy('warning_info.created_at', 'desc');
    }
}

