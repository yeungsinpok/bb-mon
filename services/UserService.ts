
import { Knex } from "knex";
import { hashPassword } from '../hash';


export class UserService{
    
    constructor(private knex:Knex){}

    async getUsers(username:string){
        return this.knex.select('*').from('user_info').where('username',username);
    }
    
    async getEmail(email:string){
        return this.knex.select('*').from('user_info').where('email',email);
    }

    async createUser(username:string, password:string, phone:number, family_name:string, given_name:string, email:string){
        return this.knex.insert({
            username:username,
            password: await hashPassword(password),
            phone:phone,
            family_name:family_name,
            given_name:given_name,
            email:email
        }).into('user_info');
    }
}

