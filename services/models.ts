export interface User{
    id?:number
    username:string
    password:string
}

export interface Message{
    id?:number
    content:string
}

export interface BabyStatus{
    id?:number
    status:string
}