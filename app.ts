import express from 'express';
// import {Request, Response} from 'express';
import path from 'path';
import expressSession from 'express-session';
import multer from 'multer';
//import {User} from './inteface';
import { isLoggedIn } from './guard';
// import {Client} from 'pg'
// import dotenv from "dotenv";
// import { readJsonConfigFile } from 'typescript';
// import jsonfile from 'jsonfile'
// import { RegisterForm } from './inteface';
import http from 'http';
import {Server as SocketIO} from "socket.io";


//import xlsx from 'xlsx';
//import {user_info} from './interfaceExcel';
//import {user_info, location_list, event_images, events, participants, wish_list} from './interfaceExcel';
// import grant from 'grant';
import Knex from 'knex';
import dotenv from 'dotenv';
import { UserService } from './services/UserService';
import { SoundService } from './services/SoundService';
import { PhotoService } from './services/PhotoService';
import { SoundController } from './controllers/SoundControllers';
import { PhotoController } from './controllers/PhotoControllers';
import { UserController } from './controllers/UserControllers';
import { MessageService } from './services/MessageService';
import { MessageController } from './controllers/MessageControllers';

import grant from 'grant';
import qrcode from 'qrcode-terminal';
import { Client as WSClient } from 'whatsapp-web.js';
dotenv.config()

const knexConfigs = require('./knexfile');
const configMode = process.env.NODE_ENV || "development";
const knexConfig = knexConfigs[configMode]
export const knex = Knex(knexConfig)


export const app = express();
const server = new http.Server(app);
export const io = new SocketIO(server);

export const wscli = new WSClient({});

// app.set('view engine', 'ejs');
app.use(express.json({ limit: "50mb" }));
// extended allow you to use more syntax
app.use(express.urlencoded({ extended: true, limit: "50mb" })) // 佢係唔識讀multipart form-data

// app.use(expressSession({
//   secret: "baby monitor",
//   resave: true,
//   saveUninitialized: true
// }))

const sessionMiddleware = expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave:true,
  saveUninitialized:true,
  cookie:{secure:false}
});

app.use(sessionMiddleware);

io.use((socket,next)=>{
  let req = socket.request as express.Request
  let res = req.res as express.Response
  sessionMiddleware(req, res, next as express.NextFunction)
});


const storageProtected = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve('./protected/uploads/babyPhoto'));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split('/')[1]}`);
  }
});
export const uploadProtected = multer({ storage: storageProtected });


export const grantExpress = grant.express({
  "defaults":{
      "origin": "http://localhost:8080",
      "transport": "session",
      "state": true,
  },
  "google":{
      "key": process.env.GOOGLE_CLIENT_ID || "",
      "secret": process.env.GOOGLE_CLIENT_SECRET || "",
      "scope": ["profile","email"],
      "callback": "/login/google"
  }
});

// Enson routes ------------------------------------------------------------------------------------------
export const userService = new UserService(knex);
export const userController = new UserController(userService);
export const messageService = new MessageService(knex);
export const messageController = new MessageController(messageService);
// Enson routes ------------------------------------------------------------------------------------------







// TK routes ----------------------------------------------------------------------------------------------
export const soundService = new SoundService(knex);
export const soundController = new SoundController(soundService);
// TK routes ----------------------------------------------------------------------------------------------




// Wilson routes ------------------------------------------------------------------------------------------
export const photoService = new PhotoService(knex);
export const photoController = new PhotoController(photoService);


io.on('connection', socket =>{
  
  socket.on('join-room', (userId)=>{
    console.log("User Id: ", userId);
    let roomId = (socket.request as any).session['user']['id'].toString() + (socket.request as any).session['user']['username'];
    console.log("Room Id:", roomId);
    if ((socket.request as any).session['user']){ 
      socket.join(roomId);
      socket.to(roomId).emit('user-connected', userId);
    }
    socket.on('disconnect', () => {
      socket.to(roomId).emit('user-disconnected', userId);
    })
  })
})

wscli.on('qr', (qr: any) => {
  qrcode.generate(qr, { small: true });
});

wscli.on('ready', async () => {
  console.log('Client is ready!');
});

wscli.initialize();
// Wilson routes ------------------------------------------------------------------------------------------

import { routes } from './routes';

app.use('/', routes);
app.use(express.static('public'));
app.use(isLoggedIn, express.static("protected"));
// app.use(express.static("protected"));

app.use((req, res) => {

  res.redirect('/404.html');
});


const PORT = 8080;
server.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}`);
})