from sanic import Sanic
from sanic.response import json
from AI_models.baby_cry_detection.rpi_main.make_prediction import sound_prediction
import base64
import wave
import os
import time

app = Sanic("Baby Monitor")


@app.route("sound", methods=["POST"])
async def sound(request):
    try:
        
        soundControllerInfo = request.json
        babySound = soundControllerInfo['babySound']
        userId = soundControllerInfo['userId']
        print('received babySound')

        decodedData = base64.b64decode(babySound)
        webmfile = './AI_models/baby_cry_detection/recording/' + 'user' + str(userId) + '.webm'
        with open(webmfile, 'wb') as file:
            file.write(decodedData)
        
        file_path = webmfile


        def convert_webm_to_avi(webm_file_path):
            new_path = create_new_avi_file_path(file_path)
            os.system(f'ffmpeg -i "{webm_file_path}" -y "{new_path}" -hide_banner')

        def create_new_avi_file_path(file_path):
            file_path = file_path.split('.')
            file_path.pop()
            file_path = '.'.join(file_path) + '.wav'
            return file_path

        convert_webm_to_avi(file_path)

        sound_file_name = 'user' + str(userId) + '.wav'
        predict_result = sound_prediction(sound_file_name)
        print('Prediction result:', predict_result)
        print()
        return json(predict_result, status = 200)

    except Exception as error:
        print(error)
        return json({"message": "sound prediction failed"}, status = 500)



if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)