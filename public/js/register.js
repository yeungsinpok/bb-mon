
let register = document.querySelector(".register_form");


register.addEventListener("submit", async function (event) {
        event.preventDefault();
        const form = event.target;
        const forObj = {
            username: form.username.value,
            password: form.password.value,
            phone: form.phone.value,
            family_name: form.family_name.value,
            given_name: form.given_name.value,
            email: form.email.value
        }
        const res = await fetch('/users',{
            method:"POST",
            headers: {
                "Content-Type": "application/json"
            }, 
            body: JSON.stringify(forObj)
        })
        const result = await res.json();
        if (res.status === 200){
            console.log(result);
            window.location = '/index.html';
        }
        else{
            document.querySelector('.alert-container')
                .innerHTML = `<div class="alert alert-warning" role="alert">
                Register failed. ${result.msg}
            </div>`;
        }
    });
