
document.querySelector('#login-form').onsubmit = async function(event){
    event.preventDefault();

    const form = event.target;

    const formObj = {
        username: form.username.value,
        password: form.password.value
    }

    const res = await fetch('/login',{
        method:"POST",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify(formObj)
    });

    const result = await res.json();
    if(res.status === 200){
        window.location = '/bb_or_mum.html';
    }else{
        document.querySelector('#alert-container')
            .innerHTML = `<div class="alert alert-warning" role="alert">
                Login Failed! ${result.msg}
          </div>`;
    }
}


