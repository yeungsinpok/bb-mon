
let register = document.querySelector(".register_form");


register.addEventListener("submit", async function (event) {
        event.preventDefault();
        const form = event.target;

        const forObj = {

            username: form.username.value,
            password: form.password.value,
            phone: form.phone.value
        }
   
        const res = await fetch('/register/google',{
            method:"POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify(forObj)
        })
       
   
        const result = await res.json();
       
        if (res.status === 200){
            window.location = '/index.html';
        }
        else{
            document.querySelector('.alert-container')
                .innerHTML = `<div class="alert alert-warning" role="alert">
                Register failed. ${res.msg}
            </div>`;
        }
    });
