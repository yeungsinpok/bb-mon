import bcrypt from 'bcryptjs';

const SALT_ROUND = 10;

export async function hashPassword(plainPassword:string){
    return await bcrypt.hash(plainPassword,SALT_ROUND);
}

export async function checkPassword(plainPassword:string,hashPassword:string){
    return await bcrypt.compare(plainPassword,hashPassword);

}

// hashPassword("c")
//     .then(console.log);

// checkPassword('12345678','$2a$10$QIfN7PJC65qJNhhOKsJgZubXWWZnbqaIRe6oQZmDyt1uwbOOgtNMK')
//     .then(console.log)