import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    
    await knex.insert([
        {
        "username":"b",
        // password is "b"
        "password":"$2a$10$GrFm2DJ87BKlsAhfbasVLe3KUS69yiAuQrdupUN7KtBFoLRiGBUxG",
        "given_name": "b",
        "family_name": "b",
        "email": "b@b",
        "phone": "12345678"},
        {
        "username":"wilson",
        // password is "wilson"
        "password":"$2a$10$AIuPtLYtWHLjx6T.35fYBedOdxp2iHZzZwg.Jvh2FvI/KNMrf2I4u",
        "given_name": "Wilson",
        "family_name": "Ho",
        "email": "cheukhang.ho@gmail.com",
        "phone": "97042847"},
        {
        "username":"tk",
        // password is "tk"
        "password":"$2a$10$/HhRPHcuvnEOTcKevtsKQ.K1dnmuYGDy/xQBHd47/WFXXjeEgJEIu",
        "given_name": "Tk",
        "family_name": "Ngan",
        "email": "ntkraphael@gmail.com",
        "phone": "90766637"},
        {
        "username":"enson",
        // password is "enson"
        "password":"$2a$10$Ww04A7h/r2TeasPes/pid.R.oXxYtYASgj0H9arBpHUi839S/u8T6",
        "given_name": "Enson",
        "family_name": "Yeung",
        "email": "yeungsinpok@gmail.com",
        "phone": "68460498"},
    
    ]).into('user_info')

};
