import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("baby_status").del();
    await knex("message").del();

    // Inserts seed entries
    await knex("baby_status").insert([
        { /*id: 1,*/ status: "Suffocation" },
        { /*id: 2,*/ status: "Off the bed" },
        { /*id: 3,*/ status: "Cry" },
    ]);

    await knex("message").insert([
        { /*id: 1,*/ content: "Duvet may nearly cover baby's nose."}, //被蓋住
        { /*id: 2,*/ content: "Baby face is covered or baby left bad!?" }, //成塊面蓋左
        { /*id: 3,*/ content: "Beware, baby is at the edge of the bed." }, //BB喺床邊，驚佢跌
        { /*id: 4,*/ content: "Baby is not in the BB mon screen~" }, //BB可能離床了/跌落床
        { /*id: 5,*/ content: "Baby is crying for 10 seconds" }, // BB喊緊
    ]);

    await knex("warning_info").insert([
        {  
            message_id: 1,
            level: 1,
            baby_status_id: 1,
            user_info_id: 1
        },   
        {  
            message_id: 2,
            level: 2,
            baby_status_id: 1,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },   
        {  
            message_id: 2,
            level: 2,
            baby_status_id: 1,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        },
        {  
            message_id: 3,
            level: 1,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 4,
            level: 2,
            baby_status_id: 2,
            user_info_id: 1
        },
        {  
            message_id: 5,
            level: 1,
            baby_status_id: 3,
            user_info_id: 1
        }
    ])};