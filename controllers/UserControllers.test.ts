//import { Client } from "pg";
import { UserService } from "../services/UserService";
import { UserController } from "./UserControllers";
import { Request, Response } from "express";
import { Knex } from 'knex'
import { hashPassword, checkPassword} from "../hash"

describe('UserControllers', () => {

    let userController: UserController;
    let req: Request;
    let res: any;
    let userService: UserService;


    beforeEach(async () => {
       userService = new UserService({} as Knex);
       userController= new UserController(userService);
       let p=await hashPassword("b")
       jest.spyOn(userService,'getUsers').mockReturnValue( Promise.resolve([{
        id: 83,
        username: 'b',
        password:p,
        given_name: 'b',
        family_name: 'b',
        email: 'b@b',
        phone: 'b',
        created_at: "2021-09-13T01:30:54.967Z",
        updated_at: "2021-09-13T01:30:54.967Z"
      }]));
    //    jest.spyOn(userService, 'getUsers')
        // jest.spyOn(userController, 'login').mockImplementation(async ()=>{res.status(200)});   
        
        //   
        res = {
            json: jest.fn(),
            status: jest.fn().mockReturnThis(),
        }
     
    });

    it("should login", async () => {
        let req:any={
            session:{},
            body: {
               username: 'b',
               password: 'b'}   
        }

        console.log(userController)
        const login = jest.spyOn(userController, 'login')
        await userController.login(req, res);
        expect(req.session['user']).toBeDefined
        expect(login).toBeCalled

    });
});