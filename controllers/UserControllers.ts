import { Request, response, Response } from 'express';
import { checkPassword, hashPassword } from '../hash';
import { User } from '../services/models';
// import crypto from 'crypto';\
import fetch from 'node-fetch';
import { UserService } from '../services/UserService';
import session from 'express-session';
import { json } from 'stream/consumers';

export class UserController {

    constructor(private userService: UserService) { }

    login = async (req: Request, res: Response) => {
        // request/response
        const { username, password } = req.body;
        // db
        let users: User[] = await this.userService.getUsers(username);
        const user = users[0]
        if (user && await checkPassword(password, user.password)) {
            // 先會set user落去session 度
            //request/response
            const {password, ...others} = user
            req.session['user'] = { ...others};
            // 4. Login 成功後，redirect 去/admin.html
            res.status(200).json({ success: true });  // URL vs PATH
        } else {
            res.status(401).json({ success: false, msg: "Username/ Password is incorrect." });
        }
    }

    loginGoogle = async (req: Request, res: Response) => {
        const accessToken = req.session?.['grant'].response.access_token;

        const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
            method: "get",
            headers: {
                "Authorization": `Bearer ${accessToken}`
            }
        });
        const result = await fetchRes.json();
        const emailInDB = await this.userService.getEmail(result.email)
        if (emailInDB.length > 0) {
            req.session['user'] = emailInDB[0]['username'];
            res.status(200).redirect('/bb_or_mum.html');
    
        } else {
            req.session['user'] = result.username
            req.session['family_name'] = result.family_name
            req.session['given_name'] = result.given_name
            req.session['email'] = result.email
            res.redirect(`/google_register.html`)
        }



    }

    registerGoogle = async (req: Request, res: Response) => {
        const { username, password, phone } = req.body;
        let getUsers = await this.userService.getUsers(username);

        if (getUsers.length > 0) {
            res.status(401).json({ msg: "username is repeat" });
            return
        }


        const hashedPassword = await hashPassword(password);
        this.userService.createUser(
            username, hashedPassword, phone, req.session['family_name'], req.session['given_name'], req.session['email']

            //  async createUser(username:string, password:string, phone:number, family_name:string, given_name:string, email:string){
        )
        res.status(200).json({ success: true });
    }


    // loginGoogle = async (req:Request,res:Response)=>{
    //     //request/response
    //     const accessToken = req.session?.['grant'].response.access_token;

    //     // external API
    //     const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
    //         headers: {
    //             "Authorization": `Bearer ${accessToken}`
    //         }
    //     });
    //     const userInfo = await fetchRes.json();
    //     // DB
    //     let users: User[] = await this.userService.getUsers(userInfo.email);
    //     let user = users[0];
    //     if (!user) {
    //         return res.redirect(`/google_register.html?family_name=${userInfo.family_name}&given_name=${userInfo.given_name}&email=${userInfo.email}`)  
    //     }
    //     //     const randomPassword = crypto.randomBytes(20).toString('hex');
    //     //     // DB
    //     //     const result = await this.userService.createUser(user.email, randomPassword );
    //     //     user = {
    //     //         username: userInfo.email,
    //     //         password: randomPassword,

    //     //         // NEED CREATE
    //     //         // username:username,
    //     //         // password: await hashPassword(password),
    //     //         // phone:phone

    //     //         // GOOGLE
    //     //         // family_name:family_name,
    //     //         // given_name:given_name,
    //     //         // email:email

    //     //         id: result[0]
    //     //     };
    //     // }

    //     // // if (checkEmail.rows.length >= 1) {
    //     // //     console.log(userData.rows[0])
    //     // //     req.session['user'] = userData.rows[0];
    //     // //     return res.redirect(`/businessCard.html`)
    //     // // } else {
    //     // //     req.session['user'] = userData.rows[0];
    //     // //

    //     // // request/response
    //     // // 先會set user落去session 度
    //     // req.session['user'] = user;
    //     // res.status(200).redirect('/admin.html');

    // }



    createUser = async (req: Request, res: Response) => {
        const { username, password, phone, family_name, given_name, email } = req.body
        // 實際應要check 埋user 個email 有無重覆

        let getEmail = await this.userService.getEmail(email);
        let getUsers = await this.userService.getUsers(username);

        if (getUsers.length > 0) {
            res.status(401).json({ msg: "username is repeat" });
            return
        }

        if (getEmail.length > 0) {
            res.status(401).json({ msg: "email is repeat" });
            return
        }

        await this.userService.createUser(username, password, phone, family_name, given_name, email);
        res.status(200).json({ success: true });
    }

    logout = async (req: Request, res: Response) => {
        delete req.session['user'];
        res.status(200).redirect('/index.html');
    }

    getCurrentUser = async (req: Request, res: Response) => {
        if (req.session['user']) {
            console.log(req.session['user'])
            res.json({
                username: req.session['user'].username
            });
        } else {
            res.status(401).json({ msg: "UnAuthorized" });
        }
    }

    getUserInfo = async (req: Request, res: Response) => {
        if (req.session['user']) {
            res.json({ userInfo: req.session['user'] })
        } else {
            res.status(401).json({ msg: "UnAuthorized" })
        }
}

}