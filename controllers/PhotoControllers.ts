import { logger } from '../logger';
import { Request, Response } from 'express';
import { PhotoService } from '../services/PhotoService';
import { io } from '../app';
import { wscli } from '../app';

export class PhotoController {

    constructor(private photoService: PhotoService) { }

    photoPredictionResult = async (req: Request, res: Response) => {
        try {
            const { suffocation, offTheBed } = req.body;
            const roomId = req.session['user']['id'].toString() + req.session['user']['username'];
            let warningInfoIds: number[];
            let warningMessages: object[];
            warningInfoIds = await this.photoService.createWarningMessage(req.session['user']['id'], suffocation, offTheBed);
            console.log('Warning info id: ', warningInfoIds);
            warningMessages = await this.photoService.getWarningMessage(warningInfoIds);
            console.log('Warning Message: ', warningMessages);
            io.to(roomId).emit('warning_message', warningMessages);
           

            // ---- Whatsapp section

            console.log('Client is ready again!');

            const phone = req.session['user']['phone'];
            console.log(`Parent's mobile number: +852${phone}`)
            const wsId = await wscli.getNumberId(`852${phone}`);
            //wsId is whatsapp id in json format - { server: 'c.us', user: '85212345678', _serialized: '85212345678@c.us' }    
            //console.log(id)
            if (!wsId) {
                console.log('No such an user.')
            } else {
                if (warningMessages) {
                    console.log("Begin monitoring BB bed.");

                    for (let warningMessage of warningMessages) {
                        if (warningMessage['level'] === 2 && warningMessage['baby_status_id'] === 1) {
                            await wscli.sendMessage(wsId._serialized, warningMessage['warning_message']);
                            
                            
                        }
                        else if (warningMessage['level'] === 2 && warningMessage['baby_status_id'] === 2) {
                            await wscli.sendMessage(wsId._serialized, warningMessage['warning_message']);
                        }
                    }
                }
            }

            // --- Whatsapp section

            res.status(200).json({success: 'Photo prediction success'});

        } catch (e) {
            logger.error(e);
            res.json({ success: 'Cannot receive photo prediction result' });
        }
    }

}