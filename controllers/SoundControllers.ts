import { logger } from '../logger';
import { Request, Response } from 'express';
import { SoundService } from '../services/SoundService';
import fetch from 'node-fetch';
import { io } from '../app';




export class SoundController {
    
    constructor(private soundService: SoundService){}

    predictionResult = async (req: Request, res: Response) => {
        try {
            
            const babySound = req.body.babySound;
            console.log("Received");
            
            const fetchRes = await fetch('http://localhost:8000/sound', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({
                    babySound: babySound,
                    userId: req.session['user']['id']
                })
            });
            const result = await fetchRes.json();

            
            if (fetchRes.status == 200) {
                if (result == 1){

                    const roomId = req.session['user']['id'].toString() + req.session['user']['username'];
                    console.log("Baby's crying. Status: ", result);
                    const warningInfoIds = await this.soundService.createWarningMessage(req.session['user']['id']);
                    console.log('Warning info id: ', warningInfoIds);
                    const warningMessages = await this.soundService.getWarningMessage(warningInfoIds);
                    console.log('Warning Message: ', warningMessages);
                    io.to(roomId).emit('warning_message', warningMessages);

                }
                else if (result == 0){                    
                    console.log("Baby is fine. Status: ", result);                    
                }
            }
            else if (fetchRes.status == 500) {
                console.log(result);
            }
            res.status(200).json({success: 'Sound prediction success'})
        } catch (e) {
            logger.error(e);
            res.json({ success: false });
        }
    }
}