import { Request, Response } from 'express';
import { MessageService } from '../services/MessageService';
import { logger } from '../logger';
//import { Server as SocketIO } from 'socket.io';
// No DB related , only on express
export class MessageController {

    constructor(private messageService: MessageService) { }


    getMessage = async (req: Request, res: Response) => {
        try {
        const userId = req.session['user']['id'];
        let result = await this.messageService.getWarningRecord(userId);
        // let message = result.rows;
        // console.log(message)
        res.status(200).json( result );
    } catch (e) {
        logger.error(e);
        res.json({ success: false });
    }}



    // updateMemos = async(req:Request,res:Response)=>{
    //     const id = parseInt(req.params.id);
    //     if(isNaN(id)){
    //         res.status(400).json({msg:"id is not an integer"});
    //         return;
    //     } 
    //     const content = req.body.content;

    //     this.memoService.updateMemo(id,content);
    //     res.json({success:true});
    // }

    // deleteMemo = async (req:Request,res:Response) =>{
    //     const id = parseInt(req.params.id);
    //     if(isNaN(id)){
    //         res.status(400).json({msg:"id is not an integer"});
    //         return;
    //     }
    //     this.memoService.deleteMemo(id);
    //     res.json({success:true});
    // }
}